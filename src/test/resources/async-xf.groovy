import groovy.transform.Field
import org.example.Async
import static org.example.Cont.*
import org.example.Cont

import java.util.concurrent.ScheduledThreadPoolExecutor
import java.util.concurrent.TimeUnit

@Async
def f1(a, b) {
    def pi = 3.14
    def r = await asyncAdd(a + b, pi)
    def r2 = r + a
    def r3 = await asyncAdd(r2, 3)
    r3 + b
}
//@Synchronized
//@TailRecursive
//@Async
def f2(a, b, Cont c) {
    def pi = 3.14
    asyncAdd(a + b, pi, new Cont({ r ->
        def r2 = r + a
        asyncAdd(r2, 3, new Cont( {
            r3 ->
                c.f(r3 + b)
        }))
    }))
}
f1(1,2,new Cont({
    println it
}))
f2(1,2,new Cont({
    println it
}))

@Field ScheduledThreadPoolExecutor pool = new ScheduledThreadPoolExecutor(10)

def asyncAdd(a, b, cont) {
    pool.schedule({
        try{
            cont.f(a + b)
        }catch (e){
            e.printStackTrace()
        }
    } as Runnable, 1, TimeUnit.SECONDS)
}
println this.class.declaredMethods.find{
    it.name == 'f1'
}.parameterTypes