package org.example

import java.util.concurrent.atomic.AtomicLong

class Cont {
    static final def seq = new AtomicLong(0)
    static public final String F_NAME = "f"
    static public final String AWAIT_NAME = "await"
    def f
    def call(Object... args){
        f(args)
    }
    Cont(f){
        this.f = f
    }
    static def await(x) {
        x
    }
    static def nextName(){
        "__cont_"+seq.getAndIncrement()
    }
}

