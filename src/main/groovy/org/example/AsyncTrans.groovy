package org.example

import org.codehaus.groovy.ast.ASTNode
import org.codehaus.groovy.ast.ClassNode
import org.codehaus.groovy.ast.MethodNode
import org.codehaus.groovy.ast.Parameter
import org.codehaus.groovy.ast.expr.*
import org.codehaus.groovy.ast.stmt.BlockStatement
import org.codehaus.groovy.ast.stmt.ExpressionStatement
import org.codehaus.groovy.ast.stmt.Statement
import org.codehaus.groovy.ast.tools.GeneralUtils
import org.codehaus.groovy.classgen.VariableScopeVisitor
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.AbstractASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation

/**
 * @author:霜之哀伤
 */
//@CompileStatic
@GroovyASTTransformation(phase = CompilePhase.SEMANTIC_ANALYSIS)
class AsyncTrans extends AbstractASTTransformation {
    static final ClassExpression contClassExpr = GeneralUtils.classX(Cont)

    @Override
    void visit(ASTNode[] nodes, SourceUnit source) {
        init(nodes, source);
        MethodNode method = (MethodNode) nodes[1];

        if (method.isAbstract()) {
            addError("Annotation " + AsyncTrans.getMY_TYPE_NAME() + " cannot be used for abstract methods.", method);
            return;
        }
        //为方法添加参数
        //变量名避免冲突
        def contName = Cont.nextName()
        def params = method.parameters as List
        params << GeneralUtils.param(contClassExpr.type, contName)
        method.parameters = GeneralUtils.params(params as Parameter[])

        BlockStatement body = method.code as BlockStatement
        //从后往前遍历，将await后的代码包装到一个闭包中
        def stmts = new LinkedList()
        int stmtCnt = body.statements.size()
        for (int i = stmtCnt - 1; i >= 0; i--) {
            def stmt = body.statements[i]
            //最后一个表达式expr是返回值，将它转化为cont.f(expr)，也就是进行回调。这里没有考虑所有场景，仅仅为了展示基本原理
            if (i == stmtCnt - 1) {
                stmt = GeneralUtils.stmt(
                        GeneralUtils.callX(
                                new VariableExpression(contName),
                                new ConstantExpression(Cont.F_NAME),
                                stmt.expression
                        ))
            }

            if ((stmt in ExpressionStatement) &&
                    (stmt.expression in DeclarationExpression) &&
                    (stmt.expression.rightExpression in StaticMethodCallExpression) &&
                    (stmt.expression.rightExpression.method == Cont.AWAIT_NAME) &&
                    (stmt.expression.rightExpression.ownerType == contClassExpr.type)) {
                //满足Cont.await 调用形式。 def x = await asyncFunc(a,b)
                stmt.expression.rightExpression.arguments.expressions[0].arguments.expressions << createContAst(stmt.expression.leftExpression, stmts)
                stmts = new LinkedList()
                //def x = await asyncFunc(a,b) => asyncFunc(a,b,{x->...})
                stmts.addFirst(GeneralUtils.stmt(stmt.expression.rightExpression.arguments.expressions[0]))
            } else {
                stmts.addFirst(stmt)
            }
        }
        body.statements = stmts
        //修复闭包的变量引用等
        repairVariableScopes(source, method)
        //println("end")
    }

    static String getMY_TYPE_NAME() {
        return MY_TYPE_NAME
    }
    static final Class MY_CLASS = Async
    static final ClassNode MY_TYPE = new ClassNode(MY_CLASS)
    static final String MY_TYPE_NAME = "@" + MY_TYPE.getNameWithoutPackage()

    //new Cont({x->stmts}),x是前面 def x = await asyncFunc(a,b)中的x,stmts是方法中该语句之后的语句。
    static Expression createContAst(left, stmts) {
        def cls = GeneralUtils.closureX(
                [GeneralUtils.param(
                        left.type,//GeneralUtils.classX(Object).type,
                        left.variable
                )] as Parameter[],
                GeneralUtils.block(stmts as Statement[])
        )
        GeneralUtils.ctorX(contClassExpr.type, GeneralUtils.args(cls))
    }

    void repairVariableScopes(SourceUnit source, MethodNode method) {
        new VariableScopeVisitor(source).visitClass(method.declaringClass)
    }
}